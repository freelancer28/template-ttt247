jQuery(document).ready(function ($) {

	// new WOW().init();

	function stickySide(idString, closest, offset) {
		if (!$(idString).length) return;
		if (!$(closest).length) return;
		if (!$(offset)) offset = 0;
		let winTop = $(window).scrollTop();
		let mainHeight = $(closest).height();
		let mainHeightOff = $(closest).offset().top;
		if (winTop + offset >= mainHeightOff && winTop + offset + $(idString).height() <= mainHeightOff + mainHeight) {
			$(idString).css({
				position: 'relative',
				top: offset + winTop - mainHeightOff + 'px'
			});
		} else {
			if (winTop + offset < mainHeightOff) {
				$(idString).attr('style', '');
			}
			if (winTop + offset + $(idString).height() > mainHeightOff + mainHeight) {
				$(idString).css({
					top: mainHeight - $(idString).height() + 'px'
				});
			}
		}
	}

	$('.tab-wrapper').each(function () {
		let $tabWrapper, $tabID;
		$tabWrapper = $(this);
		$tabID = $tabWrapper.find('.tab-link.current').attr('data-tab');
		$tabWrapper.find($tabID).fadeIn().siblings().hide();
		$($tabWrapper).on('click', '.tab-link', function (e) {
			e.preventDefault();
			$tabID = $(this).attr('data-tab');
			$(this).addClass('current').siblings().removeClass('current');
			$tabWrapper.find($tabID).fadeIn().siblings().hide();
		});
	});


	$('.main-menu-btn').on('click', function () {
		$(this).addClass('active');
		$('.main-menu').addClass('active');
	});

	$('.main-menu-overlay').on('click', function () {
		$('.main-menu-btn').removeClass('active');
		$('.main-menu').removeClass('active');
	});

	if ($('.banner-slider').length) {
		$('.banner-slider').slick({
			dots: true,
			arrows: true,
			prevArrow: '<span class="main-slide-arrow prev-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></span>',
			nextArrow: '<span class="main-slide-arrow next-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span>',
			infinite: true,
			speed: 800,
			autoplay: true,
			pauseOnHover: false,
			autoplaySpeed: 4000,
			slidesToShow: 1,
			slidesToScroll: 1,
		});
	};

	if ($('.scroll-top').length) {
		$(window).scroll(function () {
			$(this).scrollTop() > 100 ? $('.scroll-top').addClass('show') : $('.scroll-top').removeClass('show');
		});
		$('html').on('click', '.scroll-top', function () {
			$('html, body').animate({
				scrollTop: 0
			}, 'slow');
		})
	};

	$('.open-popup-btn').magnificPopup({
		removalDelay: 500,
		callbacks: {
			beforeOpen: function () {
				this.st.mainClass = "mfp-zoom-in";
			},
		},
		midClick: true
	});

	$('.open-video-popup-btn').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,
		fixedContentPos: false,
	});

	$('.gallery-item').on('click', function (e) {
		e.preventDefault();
		let $thumb = $(this).find('.mona-gallery-dys').attr('data-thumb');
		$thumb = $.parseJSON($thumb);
		if ($thumb) {
			$(this).lightGallery({
				share: false,
				actualSize: false,
				download: false,
				autoplayControls: false,
				dynamic: true,
				dynamicEl: $thumb,
				thumbnail: true,
				animateThumb: true,
				showThumbByDefault: true
			});
		}
	});

	if ($('.product-detail-slider').length && $('.product-detail-slider-nav').length) {
		$('.product-detail-slider').slick({
			dots: false,
			arrows: false,
			prevArrow: '<span class="prev-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></span>',
			nextArrow: '<span class="next-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span>',
			infinite: false,
            autoplay: false,
            fade: true,
			autoplaySpeed: 6000,
			pauseOnFocus: false,
			speed: 0,
			slidesToShow: 1,
			slidesToScroll: 1,
			asNavFor: '.product-detail-slider-nav'
		});
		$('.product-detail-slider-nav').slick({
			dots: false,
			arrows: false,
			infinite: true,
            autoplay: false,
			autoplaySpeed: 6000,
			pauseOnFocus: false,
			speed: 500,
			slidesToShow: 3,
			slidesToScroll: 1,
			vertical: true,
			asNavFor: '.product-detail-slider',
			focusOnSelect: true,
			responsive: [{
				breakpoint: 500,
				settings: {
					vertical: false,
				}
			}, ]
		});
	};

	$('.seen-post-clear-btn').on('click', function () {
		$(this).closest('.column').fadeOut(200, function () {
			$(this).hide();
		})
	});

	$('.show-sidebar-filter-btn').on('click', function () {
		$(this).toggleClass('active');
		$(this).next('.sidebar-filter').stop().slideToggle();
	});

	$(".js-select").each(function () {
		$(this).select2({
			width: "100%",
			placeholder: 'Choose..',
			containerCssClass: $(this).attr("data-container"),
		});
	});

	if ($('.filter-content').length) {
		let $number = 8;
		let more = true;
		$('.filter-content').each(function () {
			let $this = $(this);
			if ($this.find('.filter-label').length > $number) {
				$(this).addClass('has-showmore');
				$this.find(`.filter-label:gt(${$number - 1}):not(.show-more-filter-btn)`).hide();
				$(this).append('<span class="show-more-filter-btn">Show more</span>');
			}
		});
		$('.show-more-filter-btn').on('click', function () {
			if (more) {
				$(this).text('Show less');
				$(this).closest('.filter-content').find(`.filter-label:gt(${$number - 1}):not(.show-more-filter-btn)`).show();
				more = false;
			} else {
				$(this).text('Show more');
				$(this).closest('.filter-content').find(`.filter-label:gt(${$number - 1}):not(.show-more-filter-btn)`).hide();
				more = true;
			}
		});
	}

	if ($('#most-view-catalogs').length && $('.home-page-wrapper').length && window.matchMedia("(min-width: 992px)").matches) {
		let headerOffset = $('.header').outerHeight();
		if ($('body').hasClass('admin-bar')) headerOffset = $('.header').outerHeight() + $('#wpadminbar').outerHeight();
		$(window).scroll(function () {
			stickySide('#most-view-catalogs', '.home-page-wrapper', headerOffset);
		});
	}

	$('.show-hd-search-btn').on('click', function (e) {
		e.preventDefault();
		$('.hd-search-wrapper').slideToggle();
	})

	/* DANGTIN */
	$('#select-title-form .f-control').attr('autocomplete', 'off');
	$('#select-title-form .f-control').on('focus', function () {
		$(this).removeAttr('autocomplete');
	});

	$('#select-title-form .f-control').on('input', function () {
		let $this = $(this);
		let $counter = 8 - $this.val().length;
		$('#key-counter').text(`-${$counter}`);
		if ($counter <= 0) {
			$('.select-title-form .main-btn').removeClass('disabled');
			$('#key-counter').hide();
		} else {
			$('#select-title-form .main-btn').addClass('disabled');
			$('#key-counter').show();
		}
	});

	$('#select-title-form .main-btn').on('click', function () {
		if (!$(this).hasClass('disabled')) {
			$('#select-category').show();
			$(this).addClass('disabled').text('Update');
		}
	});

	$('.select-category-nav label').each(function () {
		let $this = $(this);
		if ($this.siblings('.sub-menu').length && $this.siblings('.sub-menu').find('li').length) {
			$this.addClass('have-sub');
		}
	});

	$('.select-category-nav .have-sub').on('click', function (e) {
		e.preventDefault();
		$(this).find('> input').prop('checked') ? $(this).find('input').prop('checked', false) : $(this).find('input').prop('checked', true);
		$(this).closest('.have-sub').toggleClass('show-sub');
		$(this).closest('.have-sub').siblings('.sub-menu').toggle();
		$(this).closest('li').siblings().toggle();
	});

	if ($("#dropzone").length) {
		Dropzone.autoDiscover = false;
		$("#dropzone").dropzone({
			url: "/",
			paramName: "file",
			addRemoveLinks: true,
		});
	}

	$('.tooltip').each(function () {
		if ($(this).attr('title')) {
			$(this).tooltipster();
		}
	});

	$('[name="price-check"]').on('change', function () {
		$('#price-input-num').prop('disabled', !$('#price-input [name="price-check"]').prop('checked'));
    });
    
	// if($('#tag-input .f-control').length){
	// 	let $array = [];
    //     let $tags = $('#input-tag-hiden').val();
    //     $tags.length && $array.push($tags)
	// 	$('#add-tag').on('click', function (e) {
	// 		e.preventDefault();  
    //         $tags = $('#tag-input .f-control').val().split(',');
    //         $array.push(...$tags);
    //         $('#tag-input .f-control').val('');
    //         for(let $tag of $tags){
    //             $('#tag-list').append(`<li class="li-tag">${$tag}</li>`);
    //         }
	// 		$('#input-tag-hiden').val($array);
	// 		if ($('.li-tag').length >= 5) {
	// 			$('#add-tag').addClass('disabled');
	// 		}
	// 	});
    // };
    
    if($('#tag-input .f-control').length){
        let $tagArrs, $newTagArrs, $sumTagArrs;
        $tagArrs = $('#input-tag-hiden').val().split(',').filter(el=>el!='');

        $('#tag-list').on('click', 'li', function(){
            $(this).remove();
            let $tagRemove = $(this).text().trim();
            $tagArrs = $('#input-tag-hiden').val().split(",").filter(el=>el!='');
            $tagArrs = jQuery.grep($tagArrs, function( a ) {
                return a !== $tagRemove;
            });
            $('#input-tag-hiden').val($tagArrs);
            if($tagArrs.length > 5) {
                $('#tag-input .f-control').attr('disabled', true);
                $('#tag-desc').css('color', '#ff0000');
            }
            else{
                $('#tag-input .f-control').attr('disabled', false);
                $('#tag-desc').css('color', '');
            }
        })

        
        $('#tag-input .f-control').on('input', function(){
            $('#tag-input .f-control').val() ?  $('#add-tag').removeClass('disabled') : $('#add-tag').addClass('disabled');
            $newTagArrs = $('#tag-input .f-control').val().split(',');
            if (($newTagArrs.length + $tagArrs.length) > 5){
                $('#tag-input .f-control').attr('disabled', true);
                $('#tag-desc').css('color', '#ff0000');
            }
            else{
                $('#tag-input .f-control').attr('disabled', false);
                $('#tag-desc').css('color', '');
            }
            $newTagArrs = $newTagArrs.filter(el=>el!='');
            $sumTagArrs = $tagArrs.concat($newTagArrs);
        });

		$('#add-tag').on('click', function (e) {
            e.preventDefault();
            $('#tag-list').empty();
            $('#add-tag').addClass('disabled');
            $('#tag-input .f-control').val('');
            $sumTagArrs = [...new Set($sumTagArrs)];
            for(let $sumTagArr of $sumTagArrs){
                $('#tag-list').append(`<li class="li-tag">${$sumTagArr}</li>`);
            }
            $('#input-tag-hiden').val($sumTagArrs);
            $tagArrs = $sumTagArrs;
			if ($sumTagArrs.length >= 5) {
                $('#tag-input .f-control').attr('disabled', true);
                $('#tag-desc').css('color', '#ff0000');
            }
            else{
                $('#tag-input .f-control').attr('disabled', false);
                $('#tag-desc').css('color', '');
            }
        });
    };


    function priceCalc(){
        let $totalPrice = 0;
        if($('[name="pick-your-plan"]:checked').length){
            if($('[name="pick-your-plan"]:checked').attr('data-price') !== 'free') {
                $totalPrice = parseFloat($('[name="pick-your-plan"]:checked').attr('data-price'));
            }
        };
        $('.add-ad-to-table tr:visible .custom-checkbox input:checked').each(function(){
            $totalPrice = $totalPrice + parseFloat($(this).val());
        });
        $('#input-total-price').val($totalPrice.toFixed(2));
		$('#total-price .price').text(`$${$totalPrice.toFixed(2)}`);
    };
    // priceCalc();

    function pickPlan(){
        let $selectedArr = [];
        if($('[name="pick-your-plan"]:checked').length){
            $('#website-url').css('pointer-events', 'auto');
            if (window.location.href.indexOf("/edit-post/") == -1) {
                $('.add-ad-to-table .custom-checkbox input').prop('checked', false);
            };
            $('.add-ad-to-table [name="website-url"]').prop('checked', $('[name="web-check"]').prop('checked'));
            $('.add-ad-to-table').show();
            $(`.add-ad-to-table tr`).show();
            $selectedArr = $('[name="pick-your-plan"]:checked').attr('data-selected').split(',');
            for (let $selectedItem of $selectedArr){
                $(`.add-ad-to-table [name="${$selectedItem}"]`).prop('checked', true).closest('tr').hide();
                if($selectedItem == 'website-url') {
                    $('#website-url').css('pointer-events', 'none');
                    $('[name="web-check"]').prop('checked', true);
                    $('#website-url-input').show();
                    if(!$('#website-url-input').val()){
                        let $offset;
                        if($('body').hasClass('is_admin')){
                            $offset = $('.upload-videos').offset().top - $('.header').outerHeight() - 60;
                        }
                        else{
                            $offset = $('.upload-videos').offset().top - $('.header').outerHeight();
                        }
                        $('html, body').animate({
                            scrollTop: $offset
                        }, 'slow');
                        $('#website-url-input').focus();
                    }
                }
            }
            $('.add-ad-to-table tr:visible').length ? $('.add-ad-to-table').show() : $('.add-ad-to-table').hide();
        };
        $('#select-date').each(function(){
            let $this = $(this);
            let $price = $this.find('option:selected').attr('data-price');
            $this.closest('tr').find('.custom-checkbox input').val($price);
            $this.closest('tr').find('.price').text(`$${$price}`).attr('data-price', $price);
        });
        priceCalc();
    };
    pickPlan();
    
    $('[name="web-check"]').on('change', function () {
        $(this).prop('checked') ? $('#website-url-input').show() : $('#website-url-input').hide();
        $('.add-ad-to-table [name="website-url"]').prop('checked', $(this).prop('checked'));
        priceCalc();
	});
    
	$('[name="pick-your-plan"]').on('change', function (e) {
        pickPlan();
	});

	$('.add-ad-to-table .custom-checkbox input').on('change', function (e) {
		e.preventDefault();
        let $this = $(this);
        if($this.attr('name') == 'website-url'){
            $this.prop('checked') ? $('#website-url-input').show() : $('#website-url-input').hide();
            $('[name="web-check"]').prop('checked', $this.prop('checked'));
        }
        priceCalc();
    });

    $('[name="day-top-ads"]').on('change', function(){
        let $price = $(this).find('option:selected').attr('data-price');
        $(this).closest('tr').find('.custom-checkbox input').val($price);
        $(this).closest('tr').find('.price').text(`$${$price}`).attr('data-price', $price);
        priceCalc();
    });
    
    /* END DANGTIN */
    
    // $('.category-wrapper-2 .columns.main-slider').slick({
    //     dots: false,
    //     arrows: true,
    //     prevArrow: '<span class="main-slide-arrow prev-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></span>',
    //     nextArrow: '<span class="main-slide-arrow next-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span>',
    //     infinite: true,
    //     autoplay: false,
    //     autoplaySpeed: 6000,
    //     pauseOnFocus: false,
    //     rows: 2,
    //     slidesPerRow: 4,
    //     speed: 500,
    //     responsive: [
    //         {
    //             breakpoint: 992,
    //             settings: {
    //                 slidesPerRow: 3,
    //             }
    //         },
    //         {
    //             breakpoint: 769,
    //             settings: {
    //                 slidesPerRow: 2,
    //                 dots: true,
    //                 arrows: false,
    //             }
    //         },
    //         {
    //             breakpoint: 501,
    //             settings: {
    //                 slidesPerRow: 1,
    //                 dots: true,
    //                 arrows: false,
    //             }
    //         }
    //     ]
    // });

	if ($('.header').length && $('.main').length) {
		let $header = $('.header'),
			$main = $('.main');
        $main.css('margin-top', $header.outerHeight());
        if($(window).scrollTop() > 0){
            $header.addClass('fixed');
            $main.css('margin-top', $header.outerHeight());
        }
        else{
            $header.removeClass('fixed');
            $main.css('margin-top', '');
        }
		// $(window).scrollTop() > 0 ? $header.addClass('fixed') : $header.removeClass('fixed');
		$(window).on('scroll', function () {
            // $(window).scrollTop() > 0 ? $header.addClass('fixed') : $header.removeClass('fixed');
            if($(window).scrollTop() > 0){
                $header.addClass('fixed');
                $main.css('margin-top', $header.outerHeight());
            }
            else{
                $header.removeClass('fixed');
                $main.css('margin-top', '');
            }
		})
	};
});